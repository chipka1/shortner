<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}">
        <link href="{{ mix('/css/app.css')}}" rel="stylesheet">
        <title>Shortner</title>
    </head>
    <body>
        <div id="app" class="container d-flex justify-content-center flex-column align-items-center pt-4">
            <div class="col-md-12 d-flex justify-content-center flex-column align-items-center ">
                <p>{{ session('message') ?? '' }}</p>
                @if(session('link'))
                    <p><a href="{{ session('link') }}" target="_blank">{{ session('link') }}</a></p>
                @endif
                @if ($errors->has('external_url'))
                    <p class="text-danger">
                        {{ $errors->first('external_url') }}
                    </p>
                @endif
                <form action="{{ route('link.create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <input type="text" name="external_url" max="65535" class="form-control mb-2" id="inlineFormInput" placeholder="Enter a link" value="{{ old('external_url') }}">
                           
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary mb-2">Make short link!</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12 d-flex justify-content-center flex-column align-items-center">
                @foreach ($links as $link)
                    <div class="mb-2 border-bottom w-100 text-center">
                        <a href="{{ env('APP_URL') . $link->route_name }}" target="_blank"> {{ env('APP_URL') . $link->route_name }}</a>
                        - {{ $link->clicks()->count() }} clicks
                        <h6 class="mt-0 pt-0">IPs:</h6>
                        @foreach ($link->clicks()->groupBy('ip')->get() as $click)
                            @if ($click->ip)
                                {{ $click->ip }} </br>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </body>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>
