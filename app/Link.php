<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HybridRelations;

    protected $fillable = [
        'route_name', 'external_url'
    ];

    public function clicks() 
    {
        return $this->hasMany(Click::class);
    }
}
