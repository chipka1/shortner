<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Click extends Eloquent 
{
    protected $connection = 'mongodb';
    protected $fillable = ['link_id', 'ip'];
}
