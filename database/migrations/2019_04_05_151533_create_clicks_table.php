<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('mongodb')->hasCollection('clicks')) {
            Schema::connection('mongodb')->create('clicks', function (Blueprint $collection) {
                $collection->index('link_id');
                $collection->index('ip');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::connection('mongodb')->hasCollection('clicks')) {
            Schema::connection('mongodb')->drop('clicks');
        }
    }
}
